package com.acats.kilo.tdm.util;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonParser {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JsonParser.class);

	
	public static String parseJson(URI uri) {
		return parseJson(uri, "UTF-8");
	}
	
	/**
	 * Parse the JSON content at the given URI using the specified character
	 * encoding
	 *
	 * @param uri
	 * @return
	 */
	public static String parseJson(URI uri, String encoding) {
		String json = "";
		try {
			json = IOUtils.toString(uri, encoding);
		} catch (IOException e) {
			LOGGER.error("JsonParser#ParseJson(uri, encoding) IOException: ", e);
		} catch (Exception ex) {
			LOGGER.error("JsonParser#ParseJson(uri, encoding) Exception: ", ex);
		}

		return json;
	}
	
	public static String parseJsonFromResponse(String uri , String field){
        String regToken = null;
        try {
            URL tokenUrl = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) tokenUrl.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responsecode = conn.getResponseCode();
            System.out.println("Response code is: " + responsecode);
            String inline = "";

            if (responsecode != 200)
                throw new RuntimeException("HttpResponseCode: " + responsecode);
            else {
                Scanner sc = new Scanner(tokenUrl.openStream());
                while (sc.hasNext()) {
                    inline += sc.nextLine();
                }
                System.out.println("\nJSON Response in String format");
                System.out.println(inline);
                sc.close();
            }
            conn.disconnect();
            Object obj = new JSONParser().parse(inline);

            JSONObject jo = (JSONObject) obj;


            regToken = (String) jo.get(field);

            System.out.println("RegToken is : " + regToken);

        }catch(Exception e) {
            e.printStackTrace();
        }
        return regToken;
    }
	
	public static String parseJson(File file) {
		return parseJson(file, "UTF-8");
	}

	/**
	 * Parse the JSON file using the specified character encoding
	 *
	 * @param file
	 * @return
	 */
	public static String parseJson(File file, String encoding) {
		String json = "";
		try {
			json = FileUtils.readFileToString(file, encoding);
		} catch (IOException e) {
			LOGGER.error("JsonFlattener#ParseJson(file, encoding) IOException: ", e);
		} catch (Exception ex) {
			LOGGER.error("JsonFlattener#ParseJson(file, encoding) Exception: ", ex);
		}

		return json;
	}


}
