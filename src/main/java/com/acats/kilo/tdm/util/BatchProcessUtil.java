package com.acats.kilo.tdm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class BatchProcessUtil {

	private static final Logger log = LoggerFactory.getLogger(BatchProcessUtil.class);
	private static String COLUMN_ID = "_id";

	public BatchProcessUtil() {
		super();
	}

	public Map<Integer, String[]> processRequest(DBCollection collection, BasicDBObject doc)
			throws IOException, ParseException {
		log.info("Inside Processing request " + doc.get("_id"));
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put(COLUMN_ID, doc.get(COLUMN_ID));
		DBObject dbObject = collection.findOne(whereQuery);

		int recordCount = 1;
		String[] urlList = null;
		String[][] inputList = null;
		String[][] outputList = null;
		List<Map<String, String>> paramList = new ArrayList<Map<String, String>>();
		Map<String , String[]> randomUrls = new HashMap<>();
		Pattern pattern = Pattern.compile("(\\w+)=?([^?^&]+)?");

		// fetch record count
		if (dbObject.containsField("restRequest")) {
			Object obj;
			try {
				obj = new JSONParser().parse(dbObject.get("restRequest").toString());
				JSONObject jo = (JSONObject) obj;
				recordCount = Integer.parseInt((String) jo.get("recordCount"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		// fetch UrlList
		if (dbObject.containsField("urlList")) {
			BasicDBList list = (BasicDBList) dbObject.get("urlList");
			urlList = new String[list.size()];
			for (int a = 0; a < list.size(); a++) {
				Map<String, String> queryParams = new HashMap<String, String>();
				urlList[a] = (String) list.get(a);
				Matcher matcher = pattern.matcher(urlList[a]);

				while (matcher.find()) {
					if (!(matcher.group(1).equalsIgnoreCase("http") || matcher.group(1).equalsIgnoreCase("https"))) {
						queryParams.put(matcher.group(1), matcher.group(2));

					}
				}
				paramList.add(a, queryParams);
			}
		}

		// fetch Input List
		if (dbObject.containsField("inputList")) {
			BasicDBList inplist = (BasicDBList) dbObject.get("inputList");
			inputList = new String[urlList.length][1];
			for (int a = 0; a < inplist.size(); a++) {
				BasicDBList temp = (BasicDBList) (inplist.get(a));
				if (null != temp)
					inputList[a][0] = (String) temp.get(0);
			}
		}

		// fetch output List
		if (dbObject.containsField("outputList")) {
			BasicDBList outlist = (BasicDBList) dbObject.get("outputList");
			if (outlist != null && outlist.size() > 0) {
				outputList = new String[urlList.length][1];
				for (int a = 0; a < outlist.size(); a++) {
					BasicDBList temp = (BasicDBList) (outlist.get(a));
					if (null != temp)
						outputList[a][0] = (String) temp.get(0);
				}
			}
		}

		// fetch randomizer data
		if (dbObject.containsField("data")) {
			BasicDBObject data = (BasicDBObject) dbObject.get("data");
			for (int i = 1; i <= urlList.length; i++) {
				String head = "";
				String body = "" ;
				String[] headList;
				String[] bodyList;
				String[] randomList = new String[recordCount];
				List<String> list = new ArrayList<>();
				BasicDBObject m1 = (BasicDBObject)data.get("URL-"+ i);
				if(null == m1) {
					randomUrls.put("URL-"+ i, null);
				}
				else {
					for (String k1 : m1.keySet()) {
						BasicDBList l = (BasicDBList)m1.get(k1);
						Iterator countriesIterator = l.iterator();
						while(countriesIterator.hasNext()) {
							BasicDBObject x = (BasicDBObject)countriesIterator.next();
							for (String r : x.keySet()) {
								if(r.equalsIgnoreCase("content")) {
									if(k1.equalsIgnoreCase("0")) {
									head += x.getString(r);
									head = head.trim()+";";
									}
									else {
									body += x.getString(r);
									body = body.trim()+";";
									}
								}
							}
							
						}
						}
					headList = splitRandomizerList(head);
					bodyList = splitRandomizerList(body);
					randomList = generateRandomParams(headList , bodyList , recordCount);
					randomUrls.put("URL-"+ i, randomList);
					}
				}
			
		}
		
		// generate dynamic Urls
		Map<Integer, String[]> batchresponse = new HashMap<Integer, String[]>();
		String[] res = new String[urlList.length];
		int j = 0;
		while (recordCount > 0) {
			String newUrl;
			String outParams;
			Map<String , String> outFields = new HashMap<String , String>();
			for (int i = 0; i < urlList.length; i++) {

				// Generate dynamic urls
				newUrl = generateUrls(urlList[i], inputList[i][0], outputList[i][0], paramList.get(i) , randomUrls.get("URL-"+(i+1)) , j);

				// Generate Response for new urls
				res[i] = getResponse(newUrl, outputList[i][0]);
				
				//fetch the output value from response
				outParams = chainOutputFieldToInputParam(res[i] , outputList[i][0] , outFields);
				
				if(null != outParams) {
					outFields.put(outputList[i][0], outParams);
					outParams = outputList[i][0] + "=" + outParams;
				}
				
				if(null != outParams && urlList.length > 1 && i != urlList.length-1) {
					inputList[i+1][0] = inputList[i+1][0] + "," + outParams;
				}
				
			}
			batchresponse.put(j, res);
			j++;
			recordCount--;
		}
		return batchresponse;
	}

	private String generateUrls(String urlList, String inputList, String outputList, Map<String, String> paramList , String[] randUrl , int j) {
		StringBuilder url = new StringBuilder(urlList.substring(0, urlList.indexOf("?") + 1));
		if (null != inputList && !inputList.equalsIgnoreCase("InputField")) {
			if (inputList.contains(",")) {
				url.append(inputList.replaceAll(",", "&"));
			} else if(url.toString().endsWith("?")){
				url.append(inputList);
			} else {
				url.append("&").append(inputList);
			}
		}
		if(null != randUrl) {
			if(randUrl[j].contains(":")) {
				url.append("&").append(randUrl[j]);
			} else if (randUrl[j].contains(",")) {
				url.append("&").append(randUrl[j].replaceAll(",", "&"));
			} else {
				url.append("&").append(randUrl[j]);
			}
		}
		
		Map<String, String> m = paramList;
		for (Map.Entry mapElement : m.entrySet()) {
			String key = (String) mapElement.getKey();
			String value = ((String) mapElement.getValue());
			if (!url.toString().contains(key)) {
				url.append("&").append(key).append("=").append(value);
			}
		}

		String finalUrl = url.toString();
		if(finalUrl.contains(" "))
			finalUrl = finalUrl.replace(" ", "%20");
		return finalUrl;
	}

	private String getResponse(String url, String output) throws IOException, ParseException {
		URL urlForGetRequest = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) urlForGetRequest.openConnection();
		conn.setRequestMethod("GET");
		int responseCode = conn.getResponseCode();
		String readLine = "";
		StringBuffer response = new StringBuffer();
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((readLine = in.readLine()) != null) {
				response.append(readLine);
			}
			in.close();
			System.out.println("JSON String Result " + response.toString());
		}
		return response.toString();
	}
	
	private String chainOutputFieldToInputParam(String resVal, String outField , Map<String, String> params) throws ParseException {
		String outres = null;
		if (null != outField && !outField.equalsIgnoreCase("OutField")) {
			Object obj = new JSONParser().parse(resVal);
			JSONObject jo = (JSONObject) obj;
			outres = (String) jo.get(outField);
			if(null == outres) {
				outres = params.get(outField);
			}			
		}
		return outres;
	}
	
	private String[] splitRandomizerList(String x) {
		String[] randomizer = x.trim().split(";");
		return randomizer;
	}
	
	private String[] generateRandomParams(String[] a , String[] b , int count) {
		String[] lis = new String[count];
		for(int i=0; i<count; i++) {
			lis[i] = a[0] + "=" + b[i];
		}
		return lis;
	}
}
