package com.acats.kilo.tdm.scheduling;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.acats.kilo.tdm.util.BatchProcessUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

@Component
public class ScheduledTasks {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	private static final String PROPERTIES_FILE_PATH = "files/config.properties";

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
	private static String COLUMN_STATUS = "requestStatus";

	private static String STATUS_NEW = "New";

	private static String SERVER;
	private static Integer PORT;
	private static String DATABASE;
	private static String COLLECTION;
	
	private static String COLUMN_NAME="name";
	private static String COLUMN_JSON="json";
	private static String COLUMN_ID="_id";
	private static String COLUMN_RANDOMIZER="randomizer";
	private static String STATUS_COMPLETE="Complete";
	private static String STATUS_FAILED="Failed";
	private static String STATUS_IN_PROGRESS="In Progress";

	private static DB db = null;

	public ScheduledTasks() {
		super();
		initializeMongo();
	}

	private void initializeMongo() {
		InputStream inp;
		try {
			inp = new FileInputStream(PROPERTIES_FILE_PATH);
			Properties prop = new Properties();
			prop.load(inp);
			SERVER = prop.getProperty("SERVER");
			PORT = Integer.parseInt(prop.getProperty("PORT"));
			DATABASE = prop.getProperty("DATABASE");
			COLLECTION = prop.getProperty("COLLECTION");
			MongoClient mongoclient = new MongoClient(SERVER, PORT);
			db = mongoclient.getDB(DATABASE);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Scheduled(fixedRate = 5000)
	public void reportCurrentTime() {
		log.info("Fixed Rate Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
		pollForNewRequests(db);
	}

	private String pollForNewRequests(DB db) {
		String msg = "None";
		DBCollection collection = db.getCollection(COLLECTION);
		BasicDBObject whereNewQuery = new BasicDBObject();
		whereNewQuery.put(COLUMN_STATUS, STATUS_NEW);
		BasicDBObject dbObject = (BasicDBObject)collection.findOne(whereNewQuery);
		if (null != dbObject) {
			msg = "New Request Found";
			log.info(msg);
			
			// update status of new request
			updateStatusInDb(db , dbObject , STATUS_IN_PROGRESS);
			
			// process request
			log.info("Processing request for id " + dbObject.get("_id"));
			BatchProcessUtil bap = new BatchProcessUtil();
			try {
				Map<Integer , String[]> res = bap.processRequest(collection , dbObject);
				dbObject.put("responseList", res);
				// update status to complete
				updateStatusInDb(db , dbObject , STATUS_COMPLETE);
			} catch (Exception e) {
				updateStatusInDb(db , dbObject , STATUS_FAILED);
				e.printStackTrace();
			}
		}
		return msg;
	}
	
	public void updateStatusInDb(DB db , BasicDBObject document ,String status){
		DBCollection collection = db.getCollection(COLLECTION);
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put(COLUMN_ID, document.get(COLUMN_ID));
		DBObject dbObject = collection.findOne(whereQuery);
		dbObject.put(COLUMN_STATUS, status);
		if(status.equalsIgnoreCase(STATUS_COMPLETE)) {
			List<String[]> response = new ArrayList<String[]>();
			Iterator itr = ((Map<Integer, String[]>)(document.get("responseList"))).entrySet().iterator();
			while(itr.hasNext()){
				Map.Entry columns = (Map.Entry)itr.next();
				response.add(Integer.parseInt(columns.getKey().toString()) , (String[])columns.getValue());
				dbObject.put("responseList", response);
			}
		}
		collection.update(whereQuery, dbObject);
	}
}
